let clientesObtenidos;

function getClientes(){
    const url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    // let parametros = "?$filter=Country eq 'Germany'"
    let parametros = ""

    let request= new XMLHttpRequest();  //para hacer la consulta
    
    request.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
             //validando que termine  de cargar tiene difernetes estados 1 preparando 2 enviando 3 esperando respues 4 respondida
            console.table(JSON.parse(request.responseText).value);  //btener el value
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }
    request.open("GET",`${url}${parametros}`, true);
    request.send();
 }

 function procesarClientes(){
    let JSONClientes = JSON.parse(clientesObtenidos);

    let rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    
    let divTable = document.getElementById("divTableClientes");
    let tabla = document.createElement("table");
    let tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for(let i = 0; i < JSONClientes.value.length; i++){
        let nuevaFila = document.createElement("tr");
        let columnaNombre = document.createElement("td");
        let columnaCiudad = document.createElement("td");
        let columnasBandera = document.createElement("td");
        let imgbandera = document.createElement("img");

        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        columnaCiudad.innerText = JSONClientes.value[i].City;
//        columnasBandera.innerText = JSONClientes.value[i].Country;

        if(JSONClientes.value[i].Country == "UK"){
            imgbandera.src = `${rutaBandera}United-Kingdom.png`;
        }else{
            imgbandera.src = `${rutaBandera}${JSONClientes.value[i].Country}.png`
        }       

        columnasBandera.appendChild(imgbandera);
        imgbandera.classList.add("flag");

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnasBandera);
        

        tbody.appendChild(nuevaFila);

    }

    tabla.appendChild(tbody);
    divTable.appendChild(tabla);

}