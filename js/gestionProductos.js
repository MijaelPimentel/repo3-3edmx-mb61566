// function getProducts(){
//     const url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
//     let request = new XMLHttpRequest();

//     request.onreadystatechange = function () {
//         if(this.readyState == 4 && this.status == 200){
//              console.table(JSON.parse(request.responseText).value);
//          }
//     }
//     request.open("GET",url,true);
//     request.send;
// }

let productosObtenidos;

function getProducts(){
    const url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    let request= new XMLHttpRequest();  //para hacer la consulta
    request.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
             //validando que termine  de cargar tiene difernetes estados 1 preparando 2 enviando 3 esperando respues 4 respondida
            //console.table(JSON.parse(request.responseText).value);  //btener el value
            productosObtenidos = request.responseText;
            procesarProductos();
        }
    }
    request.open("GET", url, true);
    request.send();
 }

 function procesarProductos(){
     let JSONProductos = JSON.parse(productosObtenidos);
     console.log(JSONProductos.value[0].ProductName);
     
     let divTable = document.getElementById("divTable");
     let tabla = document.createElement("table");
     let tbody = document.createElement("tbody");

     tabla.classList.add("table");
     tabla.classList.add("table-striped");

     for(let i = 0; i < JSONProductos.value.length; i++){
         let nuevaFila = document.createElement("tr");
         let columnaNombre = document.createElement("td");
         let columnaPrecio = document.createElement("td");
         let columnastock = document.createElement("td");

         columnaNombre.innerText = JSONProductos.value[i].ProductName;
         columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
         columnastock.innerText = JSONProductos.value[i].UnitsInStock;

         nuevaFila.appendChild(columnaNombre);
         nuevaFila.appendChild(columnaPrecio);
         nuevaFila.appendChild(columnastock);

         tbody.appendChild(nuevaFila);

     }

     tabla.appendChild(tbody);
     divTable.appendChild(tabla);

 }





